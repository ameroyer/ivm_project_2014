import sys, os
sys.path.append('/usr/local/lib/python')
base_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(base_dir + "/code")
import numpy as np
import visutils as ut

from tulip import *
from tulipogl import *
from tulipgui import *
#from OpenGL import GL
import ctypes.util
ctypes.CDLL(ctypes.util.find_library('GL'), ctypes.RTLD_GLOBAL)
from PyQt4 import QtGui as qg, QtCore as qc

from graph_observer import GraphSelectionObserver, HoveredListItem
from generate_graph import NeighboursGraph, query_color, database_color
from local_graph import Local_graph
from panzoom_graphicsview import Custom_GV



################### MAIN GUI FILE #######################


class Main_Window(qg.QMainWindow):
    """
    Main Window for the application
    """
    def __init__(self, iv):
        super(Main_Window, self).__init__()
        iv.setupUi(self)
        iv.tab_sim_layout.setAlignment(qc.Qt.AlignTop)
        self.local_view = Custom_GV()
        l1 = iv.local_view.layout()
        l1.addWidget(self.local_view)
        
        
        ##Create the graph
        knn_path = 'data/1NN_dists.npy'
        sameset = True
        knn = np.load(knn_path)
        self.init_graph = NeighboursGraph(knn, ut.query_dir, ut.images_dir, sameset)

        ##Create the Tulip view and Add it to the Qt interface
        nlv = self.init_graph.init_view()
        frame = nlv.viewQFrame()    
        l = qg.QHBoxLayout()
        l.addWidget(frame)
        iv.tulip_view.setLayout(l)
        
        ##Set place Holder image for global view
        pixPlaceHolder = qg.QPixmap("place_holder.png")
        defaultSize = pixPlaceHolder.size()
        self.defaultSize= defaultSize
        iv.img_label.setPixmap(pixPlaceHolder)
        iv.img_label.setScaledContents(True)

        
        ##Set place Holder Image for local_v view
        self.local_img_label = qg.QLabel()
        self.local_node_info = qg.QLabel()
        self.local_node_info.setAlignment(qc.Qt.AlignRight);
        frame = qg.QWidget()
        framel = qg.QVBoxLayout()
        framel.addWidget(self.local_img_label)
        framel.addWidget(self.local_node_info)
        frame.setLayout(framel)
        frame.setLayoutDirection(1)
        frame.setAttribute(qc.Qt.WA_TransparentForMouseEvents )
        l = qg.QFormLayout()
        l.addWidget(frame)
        self.local_view.setLayout(l)
        self.local_view.setLayoutDirection(1)

        
        #Qt Connections
        iv.save_img.clicked.connect(self.init_graph.save_img)
        iv.metric_check.stateChanged.connect(self.init_graph.metric_checked)
        iv.degree_size_check.stateChanged.connect(self.init_graph.degree_size_checked)
        iv.degree_color_check.stateChanged.connect(self.init_graph.degree_color_checked)
        iv.stop_knn.clicked.connect(self.hide_current_nn)
        iv.show_knn.clicked.connect(self.show_current_nn)
        iv.load_knn.clicked.connect(self.compute_knn)
        iv.local_display.clicked.connect(self.set_local)
        self.iv = iv
        self.scene = None
        self.islocal = False

        #Create Observer
        iv.knn_list.currentItemChanged.connect(self.highlight_node)
        self.observer = GraphSelectionObserver(iv.img_label, iv.node_info, iv.knn_list, self.init_graph.graph, defaultSize)
        self.init_graph.addObserver(self.observer)

        

    def highlight_node(self, item, old_item):
        """
        highlight a node in the global view when the corresponding List item is selected
        """
        c = self.init_graph.graph.getProperty("viewColor")
        if old_item:
            c[old_item.node] = database_color
        if item:
            c[item.node] = query_color
            
        

    def show_current_nn(self):
        """
        Show kNN for the current node for a given distance
        """
        k = self.iv.kspinBox.value()
        number = self.init_graph.graph.getIntegerProperty("id_number")
        if not self.observer.current_node:
            print "No node selected"
        i = number[self.observer.current_node]

        
        #Distance
        selected_distance = self.iv.distance_combo_local.currentIndex()
        if selected_distance == 0:
            distance = "knn"
        elif selected_distance == 1:
            distance = "knnshared"
        elif selected_distance == 2:
            distance = "shsetcor"
        elif selected_distance == 3:
            distance = "shsigm"
        elif selected_distance == 4:
            distance = "5iterate"
        elif selected_distance == 0:
            distance = "10iterate"

        #Vote
        selected_vote = self.iv.vote_combo_local.currentIndex()
        if selected_vote == 0:
            vote = "novote"
        elif selected_vote == 1:
            vote = "majority"
        elif selected_vote == 2:
            vote = "weightedmajority"
        elif selected_vote == 3:
            vote = "condorcet"
        elif selected_vote == 4:
            vote = "weightedcondorcet"
        elif selected_vote == 5:
            vote = "restrictedcondorcet"

        dists = ut.load_matrix(k, distance, vote)
        dists = dists[i,:]
        self.init_graph.show_current_nn(self.observer.current_node, k, dists)


        
    def hide_current_nn(self):
        """
        Hide knn for current node
        """
        self.init_graph.nn_color = False
        self.nn_shown = []
        self.init_graph.reset()


        
    def compute_knn(self):
        """
        Change the global view graph for a given distance
        """
        k = self.iv.kglobalspinBox.value()
        if k > 100:
            print "Not computed for k > 100"
            return

        #Save currently selected
        if self.observer.current_node:  
            number = self.init_graph.graph.getIntegerProperty("id_number")[self.observer.current_node]
        else:
            number = None

        #Choose correct distance
        
        #Distance
        selected_distance = self.iv.distance_combo_global.currentIndex()
        if selected_distance == 0:
            distance = "knn"
        elif selected_distance == 1:
            distance = "knnshared"
        elif selected_distance == 2:
            distance = "shsetcor"
        elif selected_distance == 3:
            distance = "shsigm"
        elif selected_distance == 4:
            distance = "5iterate"
        elif selected_distance == 5:
            distance = "10iterate"

        #Vote
        selected_vote = self.iv.vote_combo_global.currentIndex()
        if selected_vote == 0:
            vote = "novote"
        elif selected_vote == 1:
            vote = "majority"
        elif selected_vote == 2:
            vote = "weightedmajority"
        elif selected_vote == 3:
            vote = "condorcet"
        elif selected_vote == 4:
            vote = "weightedcondorcet"
        elif selected_vote == 5:
            vote = "restrictedcondorcet"
        dists = ut.load_matrix(k, distance, vote)

        old_graph = self.init_graph
        old_observer = self.observer

        #Create new graph
        self.init_graph = NeighboursGraph(dists, ut.images_dir, ut.images_dir, True, self.iv.metric_check.isChecked())
        nlv = self.init_graph.init_view()
        frame = nlv.viewQFrame()
        l = self.iv.tulip_view.layout()
        for i in reversed(range(l.count())): 
            l.itemAt(i).widget().setParent(None)
        l.addWidget(frame)

        #New observer
        self.observer = GraphSelectionObserver(iv.img_label, iv.node_info, iv.knn_list, self.init_graph.graph, self.defaultSize)
        self.init_graph.addObserver(self.observer)

        #delete old objects
        del old_graph
        del old_observer
        
        #Reconnect            
        self.iv.save_img.clicked.connect(self.init_graph.save_img)
        self.iv.metric_check.stateChanged.connect(self.init_graph.metric_checked)
        self.iv.degree_size_check.stateChanged.connect(self.init_graph.degree_size_checked)
        self.iv.degree_color_check.stateChanged.connect(self.init_graph.degree_color_checked)

        #Select old node
        if number:
            viewSelection = self.init_graph.graph.getBooleanProperty("viewSelection")
            viewSelection[self.init_graph.dn[number]] = True



        
    def set_local(self):
        """
        Display the local view when the tab is selected
        """
        #Get current node
        number = self.init_graph.graph.getIntegerProperty("id_number")
        if self.observer.current_node == None:
            print "No node selected!"
            return
        n = number[self.observer.current_node]

        view_mode = self.iv.mds_mode.isChecked()
        k = self.iv.k_local.value()

        
        #Distance
        distances = []
        if self.iv.knn_local2.isChecked():
            distances.append("knn")
        if self.iv.shared_local2.isChecked():
            distances.append("knnshared")
        if self.iv.shsetcor_local2.isChecked():
            distances.append("shsetcor")
        if self.iv.sigm_local2.isChecked():
            distances.append("shsigm")
        if self.iv.iter5_local2.isChecked():
            distances.append("5iterate")
        if self.iv.iter10_local2.isChecked():
            distances.append("10iterate")

        #Vote
        if self.iv.local2_vote_box.isChecked():
            if self.iv.majority_local2.isChecked():
                vote = "majority"
            elif self.iv.wmajority_local2.isChecked():
                vote = "weightedmajority"
            elif self.iv.condorcet_local2.isChecked():
                vote = "condorcet"
            elif self.iv.wcondorcet_local2.isChecked():
                vote = "weightedcondorcet"
        else:
            vote = "novote"

            #Set scene
        scene = Local_graph(self.local_img_label, self.local_node_info, self.init_graph.graph.getProperty("img_path"), self.init_graph.dn)
        scene.draw_scene(n, self.iv.mds_mode.isChecked(), distances, vote, k)
        scene.update()
        self.local_view.setScene(scene)
        self.local_view.setRenderHints(qg.QPainter.Antialiasing)
        self.local_view.fitInView(scene.sceneRect(), qc.Qt.KeepAspectRatio)

        #Delete Old Scene            
        if self.scene:
            self.scene.delete()
        self.scene = scene

            



if __name__ == "__main__":
    #Init Main Window
    from UI_mainwindow import Ui_MainWindow
    iv = Ui_MainWindow()
    mw = Main_Window(iv)
    #Open Window
    mw.setWindowTitle("Projet IVM - Flower Power")
    mw.showMaximized()
    sys.exit(qg.QApplication.instance().exec_())
