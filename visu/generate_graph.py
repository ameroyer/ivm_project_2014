import os
import sys
import numpy as np
from PyQt4 import QtGui as qg, QtCore as qc
sys.path.append('/usr/local/lib/python')
from tulip import *
from tulipogl import *
from tulipgui import *


##Some Colors Parameters
line_color = tlp.Color(50, 50, 50, 200)
light_line_color = tlp.Color(200, 200, 200)
light_node_color = tlp.Color(221, 229, 173)
local_color = tlp.Color(110,9,9)

query_color = tlp.Color(94, 171, 46)
database_color = tlp.Color(81, 96, 181)

selection_color = tlp.Color(255, 0, 0)
unit_edge_length = 210
size = 50
node_size = tlp.Size(size, size, size)






class NeighboursGraph(object):
    """
    Main Class for managing the graph and Tulip view.
    """

    
    def __init__(self, knn, query_dir, images_dir, sameset = False, edg_dist = True):
        """
        Given the path to the file containing the distances of the nn, this function inits the corresponding graph.
        Plots the initial knn graph.
        """
        self.graph = tlp.newGraph()
        
        ##Load images
        DBimage_list = sorted([f for f in os.listdir(images_dir) if f.endswith(".jpg")])
        Qimage_list = sorted([f for f in os.listdir(query_dir) if f.endswith(".jpg")])

        
        ##Create/Load graph properties        
        imgPath = self.graph.getStringProperty("img_path")
        number = self.graph.getIntegerProperty("id_number")
        status = self.graph.getStringProperty("data_type")
        distances = self.graph.getDoubleProperty("edge_distances")
        reciprocity = self.graph.getBooleanProperty("reciprocity")
        viewColor = self.graph.getColorProperty("viewColor")
        viewLabelColor = self.graph.getColorProperty("viewLabelColor")
        viewBorderColor = self.graph.getColorProperty("viewBorderColor")
        viewBorderWidth = self.graph.getDoubleProperty("viewBorderWidth")
        viewLayout = self.graph.getLayoutProperty("viewLayout")
        viewSize = self.graph.getSizeProperty("viewSize")
        viewShape = self.graph.getIntegerProperty("viewShape")
        viewLabelPosition = self.graph.getIntegerProperty("viewLabelPosition")
        viewSrcAnchorShape = self.graph.getIntegerProperty("viewSrcAnchorShape")
        


        
        ##Add Edges and Nodes from the results file (knn_path)
        query_nodes = {}
        data_nodes = {}

        if sameset:      
            for query, line in enumerate(knn):
                if not query in data_nodes:   
                    data_nodes[query] = self.graph.addNode()
                neighb = np.nonzero(line)[0]
                for n in neighb:
                    if not n in data_nodes:
                        data_nodes[n] = self.graph.addNode()

                    existing = self.graph.existEdge(data_nodes[n], data_nodes[query], True)
                    if existing.isValid():
                        viewSrcAnchorShape[existing] = tlp.EdgeExtremityShape.Arrow
                        reciprocity.setEdgeValue(existing, True)
                    else:
                        edge = self.graph.addEdge(data_nodes[query], data_nodes[n])
                        reciprocity.setEdgeValue(edge, False)
                        distances.setEdgeValue(edge, line[n]) #set edge"s distance

        else:
            for query, line in enumerate(knn):
                query_nodes[query] = self.graph.addNode()
                neighb = np.nonzero(line)[0]
                for n in neighb:
                    try:                    
                        edge = self.graph.addEdge(query_nodes[query], data_nodes[n])
                    except KeyError:
                        data_nodes[n] = self.graph.addNode()
                        edge = self.graph.addEdge(query_nodes[query], data_nodes[n])
                    distances.setEdgeValue(edge, line[n]) #set edge"s distance


                    
        #Compute degree        
        degree = self.graph.getDoubleProperty("node_degree")
        degreeParams = tlp.getDefaultPluginParameters("Degree")
        self.graph.applyDoubleAlgorithm("Degree", degree, degreeParams)

        #General Properties
        viewLabelPosition.setAllEdgeValue(tlp.LabelPosition.Top)
        viewLabelPosition.setAllNodeValue(tlp.LabelPosition.Top)
        viewSize.setAllNodeValue(node_size)
        viewBorderColor.setAllNodeValue(line_color)
        viewBorderWidth.setAllNodeValue(2)
        viewLabelColor.setAllEdgeValue(line_color)
        viewLabelColor.setAllNodeValue(tlp.Color(0,0,0))
        viewColor.setAllEdgeValue(line_color)


                    
        ##Set color and other properties to nodes
        for i in query_nodes:
           q = query_nodes[i]
           imgPath[q] = query_dir + "/" + Qimage_list[i]
           number[q] = i
           status[q] = "query"
           viewColor[q] = query_color
           viewShape[q] = tlp.NodeShape.Square
           edges = self.graph.getInOutEdges(q)
           self.graph.setEdgeOrder(q, sorted(edges, key = lambda x: distances[x]))
            
        for i in data_nodes:
           q = data_nodes[i]
           imgPath[q] = images_dir + "/" + DBimage_list[i]
           number[q] = i
           status[q] = "database"
           viewColor[q] = database_color
           #sort edges           
           edges = self.graph.getInOutEdges(q)
           self.graph.setEdgeOrder(q, sorted(edges, key = lambda x: distances[x]))
        

        #Save initial colors
        init_viewColor = self.graph.getColorProperty("init_viewColor")
        init_viewColor.copy(viewColor)
        
        #Save color function of degree
        degree = self.graph.getDoubleProperty("node_degree")
        heatMap = tlp.ColorScale([tlp.Color.Green, tlp.Color.Black, tlp.Color.Red])
        degreeColor =  self.graph.getColorProperty("degreeColor")
        for n in self.graph.getNodes():
            if degree.getNodeMax() == degree.getNodeMin():
                pos = 0
            else:
                pos = (degree[n] - degree.getNodeMin()) / (degree.getNodeMax() - degree.getNodeMin())
            degreeColor[n] = heatMap.getColorAtPos(pos)
            
        #Save State of initial grapj
        self.degree_color = False
        self.nn_color = False
        self.nn_shown = []
        self.added_edges = []
        self.dn = data_nodes
        
        ##Set Layout with edge metric activated
        fm3pParams = tlp.getDefaultPluginParameters("FM^3 (OGDF)", self.graph)
        fm3pParams["Unit edge length"] = unit_edge_length
        if edg_dist:
            fm3pParams["Edge Length Property"] = distances
        self.graph.applyLayoutAlgorithm("FM^3 (OGDF)", viewLayout, fm3pParams)


        
    def addObserver(self, observer):
        """
        Add a Graph Observer to the graph and its properties.
        """
        self.graph.addListener(observer)
        for p in self.graph.getProperties():
            self.graph.getProperty(p).addListener(observer)



            
    def init_view(self):
        """
            Init the tulip view for Qt.
        """
        tlp.saveGraph(self.graph, 'yo.tlp')
        self.view = tlpgui.createView("Node Link Diagram view", self.graph, tlp.DataSet(), False)
        params = self.view.getRenderingParameters()
        params.setSelectionColor(selection_color)
        params.setViewEdgeLabel(True)
        params.setViewArrow(True)
        self.view.setRenderingParameters(params)
        #interactors = []
        #interactors.append(tlpgui.createInteractor("InteractorNavigation"))
        #interactors.append(tlpgui.createInteractor("InteractorSelection"))
        #self.view.setInteractors(interactors)
        self.view.setOverviewVisible(False)
        self.view.setQuickAccessBarVisible(False)        
        return self.view



    def save_img(self):
        """
        Open a QFileDialog for saving the current Tulip as an image + tlp file.
        """
        cur_dir = os.path.dirname(os.path.realpath(__file__))
        dest = str(qg.QFileDialog.getSaveFileName(None, 'Save file', 
                cur_dir + "/snapshot.png", selectedFilter='*.jpg;*.jpeg;*.png;*.bmp;*.gif'))
        self.view.saveSnapshot(dest, 1920, 1080)
        tlp.saveGraph(self.graph, dest.rsplit('.', 1)[0] + '.tlp')


        
    def metric_checked(self, state):
        """
        Turn on/off the respect of distances on edges.
        """
        distances = self.graph.getDoubleProperty("edge_distances")
        viewLayout = self.graph.getLayoutProperty("viewLayout")

        fm3pParams = tlp.getDefaultPluginParameters("FM^3 (OGDF)", self.graph)
        fm3pParams["Unit edge length"] = unit_edge_length
        
        if state == qc.Qt.Checked:
            fm3pParams["Edge Length Property"] = distances
            
        self.graph.applyLayoutAlgorithm("FM^3 (OGDF)", viewLayout, fm3pParams)
        self.view.centerView()

        

    def degree_size_checked(self, state):
        """
        Turn on/off the vizualisation of nodes' degree (by size)
        """
        
        if state == qc.Qt.Checked:#If activated, apply metric
            degree = self.graph.getDoubleProperty("node_degree")
            viewSize = self.graph.getSizeProperty("viewSize")
            sizeMappingParams = tlp.getDefaultPluginParameters("Size Mapping", self.graph) #Metric Mapping ?
            sizeMappingParams["property"] = degree
            sizeMappingParams["min size"] = size / 3
            sizeMappingParams["max size"] = size
            self.graph.applySizeAlgorithm("Size Mapping", viewSize, sizeMappingParams)
        else:
            viewSize = self.graph.getSizeProperty("viewSize")
            viewSize.setAllNodeValue(node_size)


    

    def degree_color_checked(self, state):
        """
        Turn on/off the vizualisation of nodes' degree (by color)
        """
        
        if state == qc.Qt.Checked:#If activated, apply metric
            self.degree_color = True
            viewColor = self.graph.getColorProperty("viewColor")
            degreeColor =  self.graph.getColorProperty("degreeColor")
            
            to_browse = self.nn_shown
            if len(to_browse) == 0:
                to_browse = self.graph.getNodes()
            for n in to_browse:
                viewColor[n] = degreeColor[n]
        else:
            #Reinit old colors
            self.degree_color = False
            self.reset()




            
    def show_current_nn(self, node, k, dists):
        """
        Highlights k-nn of the current node
        """
        for e in self.added_edges:
            self.graph.delEdge(e)
            self.added_edges = []

            
        if node != None and k > 0:
            current_color =  self.graph.getColorProperty("degreeColor")  if self.degree_color else self.graph.getColorProperty("init_viewColor")               
            viewLabel = self.graph.getStringProperty("viewLabel")
            viewColor = self.graph.getColorProperty("viewColor")
            viewBorderColor = self.graph.getColorProperty("viewBorderColor")
            distances = self.graph.getDoubleProperty("edge_distances")
            viewLabel.setAllNodeValue("")
            viewColor.setAllNodeValue(light_node_color)
            viewBorderColor.setAllNodeValue(light_line_color)
            viewColor.setAllEdgeValue(light_line_color)
            viewColor[node] = selection_color
            viewBorderColor[node] = line_color
            self.nn_shown = []
            self.added_edges = []

            #Browse k-NN
            neighb = np.nonzero(dists)[0]
            neighb = sorted(neighb, key = lambda x: dists[x])[:k]    
            data_nodes = self.dn
            viewLabel.setAllEdgeValue("")
            for i, n in enumerate(neighb):
                neighbour = data_nodes[n]
                existing = self.graph.existEdge(neighbour, node, False)
                if existing.isValid():
                    edge = existing
                else:
                    edge = self.graph.addEdge(node, neighbour)
                    self.added_edges.append(edge)
                    distances[edge] = dists[n]
                    
                viewColor[edge] = local_color
                viewColor[neighbour] = current_color[neighbour]
                viewBorderColor[neighbour] = line_color
                viewLabel[neighbour] = str(i+1) + "-th"
                viewLabel.setEdgeValue(edge, str(dists[n]))
                self.nn_shown.append(neighbour)
            nnColor = self.graph.getColorProperty("nnColor")
            nnColor.copy(viewColor)
            i = self.graph.getColorProperty("init_viewColor")   
            for g in self.nn_shown:
                nnColor[g] = i[g]
            self.nn_color = True

        


    def reset(self):
        """
            Reset graph's color and selection
        """
        for e in self.added_edges:
            self.graph.delEdge(e)
            self.added_edges = []
            
        viewLabel = self.graph.getStringProperty("viewLabel")
        viewLabel.setAllNodeValue("")
        viewColor = self.graph.getColorProperty("viewColor")
        viewBorderColor = self.graph.getColorProperty("viewBorderColor")
        if self.degree_color: #If degree mode activated
            viewColor.copy(self.graph.getColorProperty("degreeColor"))
        elif self.nn_color: #If nn restricted view activated
            viewColor.copy(self.graph.getColorProperty("nnColor"))
        else: #If no special color mode activated
            viewColor.copy(self.graph.getColorProperty("init_viewColor"))
        viewBorderColor.setAllNodeValue(line_color)
        
                
            
