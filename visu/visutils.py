from PyQt4 import QtGui as qg, QtCore as qc
import numpy as np
import os
import sys
base_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
sys.path.append(base_dir + "/code")


### Labels and Colors for local graph
labels = ['kNN', 'kNN-shared', 'kNN-shared sigmoid', 'kNN-shared set cor', 'Majority Vote', 'Weighted Majority Vote']
colors = [qg.QColor(68, 200, 255), qg.QColor(116, 39, 232), qg.QColor(255,56,63), qg.QColor(232,153,59), qg.QColor(126, 255, 36), qg.QColor(29,29,135)]
headers = [ """<font color="%s">%s</font> """ %(c.name(), l) for c,l in zip(colors, labels)]
full_header = "<br />".join(headers)
center_node_color = qg.QColor(0,255,0)



### Local Graph Colors
line_color = qg.QColor(47,47,47)
selected_color = qg.QColor(166, 30, 32)

query_color = qg.QColor(94, 171, 46)
query_hover_color = qg.QColor(57, 102, 38)

database_color = qg.QColor(81, 96, 181)
database_hover_color = qg.QColor(39, 49, 105)


### Local Dirs
base_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
images_dir = base_dir + "/visu/Database"
query_dir = base_dir + "/visu/Database"
database_name = base_dir + "/Database.h5"


#### Base Matrix
coords = np.load('data/points_coords.npy')
all_dists  = np.load('data/symmetrized_2900NN_dists.npy')
try:
    base_shared = np.load('data/base/100NN_shared.npy')
except:
    base_shared = None

try:
    base_setcor = np.load('data/base/100NN_shared_set_cor.npy')
except:
    base_setcor = None

try:
    base_sigm = np.load('data/base/100NN_shared_sigm.npy')
except:
    base_sigm = None



#### Precomputed Distances 
knn = np.load('data/generated/100NN_knn_novote.npy')
knnsh = np.load('data/generated/100NN_knnshared_novote.npy')
knnshcor = np.load('data/generated/100NN_shsetcor_novote.npy')
knnshsigm = np.load('data/generated/100NN_shsigm_novote.npy')
iterate5 = np.load('data/generated/100NN_iterate5_novote.npy')
iterate10 = np.load('data/generated/100NN_iterate10_novote.npy')


###### Return matrices if already loaded
def precomputed_matrices(distance, vote):
    global knn, knnsh, knnshcor, knnshsigm, knnmajority, knnweighted, base_sigm, base_setcor, base_shared
    dists = None
    if vote == "novote":
        if distance == "knn":
            dists = knn
        elif distance == "knnshared":
            dists = knnsh
        elif distance == "shsetcor":
            dists = knnshcor
        elif distance == "shsigm":
            dists = knnshsigm
        elif distance == "5iterate":
            dists = iterate5
        elif distance == "10iterate":
            dists = iterate10
            
    return dists
        
        



#### Generate matrix from code if not existing
def generate_matrix(k, distance, vote):
    from compute_knn_dist import compute_knn, compute_k_shared, election_with_majority, election_with_weighted_majority, election_with_condorcet, election_with_weighted_condorcet, election_with_weighted_restricted_condorcet
    global database_name, query_dir, images_dir
    ####1.Precompute distance no voting
    #If already loaded
    print "Loading", distance, "novote"
    dists = None
    if k <= 100:
        dists = precomputed_matrices(distance, "novote")

    if dists == None:
        #Else search in folder if precomputed
        folder = "data/generated/"
        found = False
        for f in os.listdir(folder):
            if f.endswith('.npy'):
                k_ = int(f.split('NN_')[0])
                _, distance_, vote_ = f.split('_')
                vote_ = vote_.rsplit('.',1)[0]
                if k <= k_ and distance == distance_ and "novote" == vote_:
                    print "Loading precomputed No Vote matrix"
                    dists = np.load(folder + f)
                    found = True
                    break
        #If not precomputed: compute
        if not found:
            print "Precomputing No Vote matrix"
            if distance == "knn":                
                dists = compute_knn(database_name, query_dir, images_dir, k, True)
                
            elif distance == "knnshared":                
                dists = base_shared
                dists = compute_k_shared(dists, k)
                
            elif distance == "shsetcor":      
                dists = base_setcor
                dists = compute_k_shared(dists, k)

            elif distance == "shsigm":      
                dists = base_sigm
                dists = compute_k_shared(dists, k)

            
            elif distance == "5iterate" or distance == "10iterate":
                print "Error, iterative methods should be precomputed (too long to compute on the fly"
                dists = compute_knn(database_name, query_dir, images_dir, k, True)
            print "Saving NoVote matrix"
            np.save(folder + str(k) + "NN_" + distance + "_novote.npy", dists)

    ###Voting
    print "Running", vote
    if vote == "majority":
        dists = election_with_majority(dists, k = 100, filtred = k)
    elif vote == "weightedmajority":
        dists = election_with_weighted_majority(dists, k = 100, filtred = k)
    elif vote == "condorcet":
        dists = election_with_condorcet(dists, k = 100,  listSize= 100, k_winners = k)
    elif vote == "weightedcondorcet":
        dists = election_with_weighted_condorcet(dists, k = 100,listSize= 100, k_winners = k)
    elif vote == "restrictedcondorcet":
        dists = election_with_weighted_restricted_condorcet(dists, k = 100,listSize= 100, k_winners = k)
    
    return dists



######### Load Matrix for given k, distance and vote parameters
def load_matrix(k, distance, vote):
    #If already loaded
    print "load", distance, vote
    dists = None
    if k <= 100:
        dists = precomputed_matrices(distance, vote)
    if dists == None:
        #Else search in folder if precomputed
        folder = "data/generated/"
        found = False
        for f in os.listdir(folder):
            if f.endswith('.npy'):
                k_ = int(f.split('NN_')[0])
                _, distance_, vote_ = f.split('_')
                vote_ = vote_.rsplit('.',1)[0]
                if k <= k_ and distance == distance_ and vote == vote_:
                    print "Loading precomputed matrix", f
                    dists = np.load(folder + f)
                    found = True
                    break
        #If not precomputed: compute
        if not found:
            print "Precomputing matrix"
            dists = generate_matrix(k, distance, vote)
            print "Saving matrix"
            np.save(folder + str(k) + "NN_" + distance + "_" + vote + ".npy", dists)

    #Limit to k
    result = np.copy(dists)
    result[result == 0] = sys.maxint
    index = np.argsort(result, axis = 1)
    del result
    result = np.copy(dists)
    for i, ki in enumerate(index):
        result[i,ki[k:]] = 0
    return result
