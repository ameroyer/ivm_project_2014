"""
PyQt custom widget for the local view
"""
from PyQt4 import QtGui as qg, QtCore as qc

class Custom_GV(qg.QGraphicsView):
    def __init__(self):
        super(Custom_GV, self).__init__()
        self.setDragMode(qg.QGraphicsView.ScrollHandDrag);
        self.scaleFactor = 1.15
        self.setHorizontalScrollBarPolicy(qc.Qt.ScrollBarAlwaysOff )
        self.setVerticalScrollBarPolicy(qc.Qt.ScrollBarAlwaysOff)

    def wheelEvent(self, event):
        self.setTransformationAnchor(qg.QGraphicsView.AnchorUnderMouse)
        if (event.delta() > 0):
            self.scale(self.scaleFactor, self.scaleFactor)
        else:
            self.scale(1.0 / self.scaleFactor, 1.0 / self.scaleFactor)
