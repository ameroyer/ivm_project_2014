import sys
sys.path.append('/usr/local/lib/python')
from tulip import *
from tulipogl import *
from tulipgui import *
from PyQt4 import QtGui as qg, QtCore as qc


title_style = """<span style = "font-size:18px; color: rgb(0, 0, 128); font-variant: small-caps; font-weight: bold; font-family: "Courier"">%s: </span>"""
k_limit = 25


class HoveredListItem(qg.QListWidgetItem):
    def __init__(self, icon, txt, node):
        super(HoveredListItem, self).__init__(icon, txt)
        self.node = node


##Graph Observer Object
class GraphSelectionObserver(tlp.Observable):
    
  def __init__(self, label, info, knnlist, graph, size):
    """
    Initialization Function
    """
    tlp.Observable.__init__(self)
    self.label = label #label for displaying image
    self.info = info   #Text Edit for displaying info on the nodes
    self.size = size   #size of the image
    self.graph = graph #main graph to observe
    self.current_node = None
    self.knnlist = knnlist
    

    
  def set_info_txt(self, src):
    """
      Change info textEdit
    """
    global k_limit
    #Get Properties
    idn = self.graph.getProperty("id_number")
    node = str(idn.getNodeValue(src))
    status = self.graph.getProperty("data_type").getNodeValue(src)
    path = self.graph.getProperty("img_path").getNodeValue(src)
    degree = str(self.graph.deg(src))
    viewLabel = self.graph.getStringProperty("viewLabel")
    distances = self.graph.getDoubleProperty("edge_distances")

    #Set number, degree and path
    txt = "<p>" + title_style%("Number") + node + "</p><p>" + title_style%("Set") + status + "</p><p>" + title_style%("Degree") + degree +  "</p><p>" + title_style%("Path") + path +  "</p>"
   

    #Set neighbouring nodes/edges and labels
    self.knnlist.clear()
    count = 0
    reciprocity = self.graph.getBooleanProperty("reciprocity")
    #viewLabel.setAllEdgeValue("")
    for edge in self.graph.getInOutEdges(src):
      if count > k_limit:
        break
      count += 1
      neighbour = self.graph.opposite(edge, src)
      arrow = "-"
      if self.graph.target(edge) == neighbour:
        arrow += ">"
      elif self.graph.source(edge) == neighbour:
        arrow = "<" + arrow
      if reciprocity[edge]:
        arrow = "<->"
      #viewLabel.setEdgeValue(edge, "%.2f"%distances[edge])
      path = self.graph.getProperty("img_path").getNodeValue(neighbour)
      
      item = HoveredListItem(qg.QIcon(path), 'Node %d (%.3f)  %s'%(idn[neighbour], distances[edge], arrow), neighbour)
      item.setToolTip('<html><img src="%s"/></html>'%path);
      self.knnlist.addItem(item)
    self.info.setHtml(txt)
    

    
  def treatEvent(self, event):
    """
    Handling Events Function
    """
    
    ##On changing selection
    if isinstance(event, tlp.PropertyEvent) and event.getProperty().getName() == "viewSelection":
      if event.getType() == tlp.PropertyEvent.TLP_AFTER_SET_NODE_VALUE:
        ##Select Node -> Display Info in info tab
        src = event.getNode()
        viewSelection = event.getProperty()
        if viewSelection[src]:
          path = self.graph.getProperty("img_path").getNodeValue(src)
          #set img
          self.current_node = src
          img = qg.QImage(path).scaled(self.size)
          self.label.setPixmap(qg.QPixmap.fromImage(img))
          #set txt info
          self.set_info_txt(src)
