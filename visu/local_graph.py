import numpy as np
from PyQt4 import QtGui as qg, QtCore as qc
import visutils as ut

from tulip import *
from tulipogl import *
from tulipgui import *
from math import cos, sin

class NodeItem(qg.QGraphicsItem):
    """
    Defines a node in the local graph
    """
    unit_width = 0.06 * 100    
    
    def __init__(self, x, y, indices, dists, img_label, dist_info, width = 0):
        """
        Initialization function
        """
        super(NodeItem, self).__init__()
        self.img_label = img_label
        self.node_info = dist_info
        self.x = x 
        self.y = y
        self.width = self.unit_width * width
        self.indices = indices
        self.dists = dists
        self.setAcceptHoverEvents(True)
        self.hovered = False


        
    def add_dist(self, i, d, mode_MDS = True):
        """
        Add a new distance associated to the node
        """
        self.indices.append(i)
        self.dists.append(d)
        if mode_MDS:
            self.width += self.unit_width
        else:
            self.width = self.unit_width



        
    def boundingRect(self):
        """
        Bouding Rectangle of the Node
        """
        return qc.QRectF(self.x - self.width, self.y - self.width, 2* self.width, 2* self.width)



    
    def paint(self, painter, option, widget):
        """
        Graphic aspect
        """
        if self.hovered:
            painter.setPen(ut.selected_color)
            painter.setBrush(qg.QBrush(qg.QColor(110,110,110)))
            if len(self.indices) == 0:    
                painter.drawRect(self.boundingRect())
            else:
                painter.drawEllipse(self.boundingRect())

        else:
        #if central node
            if len(self.indices) == 0:    
                painter.setPen(ut.line_color)
                painter.setBrush(qg.QBrush(ut.center_node_color))
                painter.drawRect(self.boundingRect())
            elif len(self.indices) == 1:
                painter.setPen(ut.line_color)
                painter.setBrush(qg.QBrush(ut.colors[self.indices[0]]))
                painter.drawEllipse(self.boundingRect())            
            #other nodes        
            else:             
                unit_angle = float(360 * 16) / len(self.indices)
                for j, i in enumerate(self.indices):         
                    painter.setPen(ut.line_color)
                    painter.setBrush(qg.QBrush(ut.colors[i]))
                    painter.drawPie(self.boundingRect(), j*unit_angle, unit_angle)


                
    def hoverEnterEvent(self, event):
        self.hovered = True
        self.update()
        
    def hoverLeaveEvent(self, event):
        self.hovered = False
        self.update()

        
    def mousePressEvent(self, event):
        """
            Reimplementation of press event: set to writable
        """
        if event.button() == qc.Qt.LeftButton:  
            self.img_label.setPixmap(self.pixmap)   
            self.node_info.clear()
            self.node_info.setText(self.txt)
            

    def define_pixmap(self, path, s, dst, headers):
        #Define QPixMap
        img = qg.QImage(path).scaled(qc.QSize(s,s))
        img = qg.QPixmap.fromImage(img)
        pix = qg.QPixmap(qc.QSize(s,s))
        pix.fill(qc.Qt.transparent);
        painter = qg.QPainter(pix)
        painter.setOpacity(0.6)
        painter.drawPixmap(0, 0, img);
        painter.end()
        del painter
        self.pixmap = pix
        #Define Info Text
        if len(self.indices) == 0:
            self.txt = "<br />".join(["""<font color="%s">%s</font> """ %(ut.colors[i].name(), h) for i, h in enumerate(headers)])
        else:
            self.txt = "<br />".join(["""<font color="%s">%s</font> """ %(ut.colors[i].name(), headers[i]) + ": " + str(d) for i, d in zip(self.indices, self.dists)])
            self.txt += "<br />" + "kNN-D: " + str(dst)


            


class Local_graph(qg.QGraphicsScene):
    """
    Create the scene for the local graph
    """
    
    def __init__(self, img_label, node_info, pathprop, dn, parent = None):        
        super(Local_graph, self).__init__(parent)
        self.path_prop = pathprop
        self.img_label = img_label
        self.node_info = node_info
        self.graph_nodes = dn
        self.img_label


        
    def draw_scene(self, src, mode_mds, distances, vote, k):
        #Precomputed distances (until k = 100)
        self.clear()
        dists = []
        for d in distances:
            dists.append(ut.load_matrix(k,d,vote)[src,:])
        nodes = {}

        #Set scale
        if not mode_mds:
            scale = 150
            for d in dists:
                d /= np.amax(d)
            x, y = 0, 0
        else:
            scale = 450
            x, y = ut.coords[src, :] * scale

            
        #Initial Node
        nd = NodeItem(x, y, [], [0]*len(dists), self.img_label, self.node_info, width = 2)
        nodes[src] = nd
        self.addItem(nd)

        
        #Add Neighbours
        for i, d in enumerate(dists):
            neighbours = sorted(np.nonzero(d)[0], key = lambda x: d[x])[:k]
            for n in neighbours:
                if not n in nodes:
                    if mode_mds:
                        x, y = ut.coords[n, :] * scale
                    else:
                        x, y = scale, scale
                    nd = NodeItem(x, y, [], [], self.img_label, self.node_info)
                    self.addItem(nd)
                    nodes[n] = nd
                nodes[n].add_dist(i, d[n], mode_mds)

        
        #Coordinates
        if not mode_mds:
            unit_angle = 360.0 / len(nodes)
            for n in nodes:
                o = nodes[n]
                d = [1] * len(dists)
                for k, t in enumerate(o.dists):
                    d[k] = t
                r = np.mean(d)
                co = cos(n * unit_angle)
                si = sin(n * unit_angle)
                o.x *= r*co
                o.y *= r*si

        for n in nodes:
            nd = nodes[n]
            nd.define_pixmap(self.path_prop[self.graph_nodes[n]], 150, ut.all_dists[src,n], distances)
        self.nodes = nodes


    def delete(self):
        """
        Reset
        """
        for o in self.nodes.values():
            del o.pixmap
        self.node_info.clear()
        self.img_label.setPixmap(qg.QPixmap())
    

   
    

