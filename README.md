## IVM Projet - 2014 2015 - README

### Installation
#### Tulip et vue graphique
Le projet a été réalisé en utilisant [Tulip version 4.6](http://tulip.labri.fr/TulipDrupal/), mais toute version supérieure à la 4.4 devrait convenir. L'interface graphique nécessite aussi les [bindings Python pour Qt 4](http://www.riverbankcomputing.co.uk/software/pyqt/intro) (voir paquets ``tulip`` et ``python-qt4``) pour certaines distributions linux.


#### Recherche de plus proches voisins
Pour certaines requêtes utilisateur, l'interface graphique peut avoir besoin de calculer une matrice de plus proche voisins pour mettre à jour l'affichage. Dans ce cas, on suppose que les dossiers *visuZ* et *code* ont été placés à la racine du dossier IVM (initialement fourni par le projet), et que la commande ``source local/XX.env`` a été effectuée avant de lancer l'interface, afin d'initialiser correctement les chemins.

## Préparation de la base de données
La base de données utilisée pour le projet contient 2901 images de la base d'origine; les descripteurs pré-calculés sont dans le fichier ``Database.h5``; Cependant l'interface graphique utilise aussi les images pour les afficher; pour créer le dossier contenant les images, on peut utiliser le script ``initialize_database.sh``, qui copie les bonnes images du dossier *Copydays* vers le dossier *visu/Database* (en supposant que l'arborescence des dossiers est bien respectée).



### Interface Graphique
L'interface graphique se lance simplement depuis le dossier *visu* par la commande ``python main_tulip.py``.

![Screen1](Screens/main_preview.png)

#### Vue Globale
L'onglet *global* à  gauche, permet d'afficher le graphe entier des plus proches voisins dans une vue gérée par Tulip. Initialement, le graphe représenté est le graphe des 1 plus proche voisin où chaque image de la base de données a été utilisée comme une requête contre toutes les autres. Les plus proches voisins ont été calculés avec les scripts initialement fournis dans le projet et les descripteurs gists.

L'orientation des arêtes représente le sens de la relation: ainsi une arête **a -> b** indique que **b** a été renvoyé comme le plus proche voisin de **a**. Une double flèche indique que la relation est réciproque.
La longueur des arêtes reflète normalement la distance entre deux points; lorsqu'il n'y a pas d'information de distances entre deux points (i.e., pas d'arêtes), leur placement dans le graphe se fait de manière indépendante (cette organisation du graphe est gérée par Tulip).

![Screen2](Screens/global_graph.png)

#### Choix des Distances et méthodes de votes
L'onglet simulation (à  droite) permet de modifier legèrement le graphe global (taille et couleur des nœuds selon leur degré par exemple). Il permet aussi de choisir les paramètres de recherche de plus proches voisins pour la génération du graphe global.

![Screen3](Screens/Simulation.png)
    
En plus du paramètre k (nombre de plus proches voisins), on peut sélectionner une distance, éventuellement suivie d'une certaine méthode de vote pour générer les kNN finaux.

  * **Note 1:** en pratique, il vaut mieux choisir k = 1; pour des valeurs plus grandes, les graphes deviennent très vite illisibles.

  * **Note 2:** les distances sans vote sont toutes pré-calculées (dossier visu/data/base et visu/data/generated). Les méthodes avec vote sont calculées par l'interface si elle ne trouve pas la matrice correspondante; certaines (e.g Condorcet) prennent quelques minutes; l'avancement des calculs s'affichent dans le terminal.

**Choix de la distance:**

  * *k-NN*: Distance initiale (avec descripteur gists)
  * *k-NN Shared*: à partir de la matrice des k-nn, on calcule la valeur " k - (cardinal de l'ensemble d'intersection entre les voisins de i et les voisins de j)". On cherche ensuite les plus proche voisins à l'aide de cette distance.
  * *Set_Cor:* applique la formule de set corrrelation à k-NN Shared.
  * *Shared Sigmoid*: applique la fonction sigmoïde à k-NN Shared.
  * *5NN-iterate* et *10NN-iterate*: construit le graphe réciproque et utilise la méthode d'un des papiers que nous avons lus pour la synthèse qui consiste à aller voir dans les voisins des voisins s'il y en a un plus proche.

**Choix de la méthode de vote:**

  * *None*: Pas de vote appliqué
  * *Majoriy Vote*: vote à la majorité; s'applique individuellement à chacun des nœuds du graphe. Pour un nœud i, un ensemble d'électeurs est défini : il s'agit des 100-nn du nœud i (selon la distance fournie en entrée). Chaque électeur augmente de 1 le score de son plus proche voisin. Le nœud ayant récolté le plus de voix est assigné comme "meilleur" voisin du nœud i, le second nœud ayant reçu le plus de voix comme le deuxième "meilleur" voisin etc.
  * *Weighted Majority Vote*: le vote à la majorité pondérée s’exécute de la même manière que le vote à la majorité, sauf que chaque électeur augmente de 1/(1+x) le score de son plus proche voisin, où x est la distance de l'électeur au nœud i.
  * *Condorcet*: le vote de Condorcet a le même ensemble d'électeurs que les votes précédents. Chaque électeur vote entre chaque paire de nœuds du graphe en augmentant de 1 le score du gagnant de chaque duel. Le candidat remportant le plus de duel est assigné comme le "meilleur" voisin du nœud i, le second candidat remportant le plus de duel comme le second "meilleur" voisin etc.
  * *Weighted Condorcet*: le vote de  Condorcet pondéré s’exécute de la même manière que le vote Condorcet, sauf que chaque électeur augmente de exp(-x^2*100) le score de son plus proche voisin, où x est la distance de l'électeur au nœud i.
  * *Restricted Condorcet*: le vote de  Condorcet pondéré restreint s’exécute de la même manière que le vote Condorcet pondéré, sauf que l'ensemble des candidats pour les duels est restreint à l'ensemble des électeurs, c'est à dire les k-nn du nœud i.

![Screen 4](Screens/otherview.png)

#### Sélection de Noeuds
En cliquant sur la deuxième icone du Menu Tulip (rectangle bleu), on peut sélectionner des nœuds sur le graphe. Lorsque qu'un nœud est sélectionné, des informations supplémentaires s'affichent dans le widget Qt à doite; notamment: l'image correspondant au nœud, son degré dans le graphe, ainsi que sa liste des plus proches voisins.
Dans cette liste, on affiche le numéro de chaque nœud voisin, leur distance (selon la distance actuellement affichée par le graphe global), ainsi que l'orientation de l'arête reliant les nœuds. Enfin, la liste est trié du voisin le plus proche au plus éloigné.

![Screen5](Screens/nodeselec.png)

On peut aussi, à partir de ce menu, mettre en surbrillance les plus proches voisins du nœud selon les différentes distances et méthodes de vote sus-citées. Le choix des paramètres est identique que celui décrit précédemment. Les voisins renvoyés ainsi que les arêtes sont mis en surbrillance par rapport aux autres nœuds; sur les arêtes on indique aussi la distance entre les deux nœuds (selon la distance qui a été sélectionnée, qui n'est pas forcément la distance utilisée pour le graphe global).

![Screen6](Screens/nodeshow.png)

#### Vision locale à un noeud
Afin de pouvoir comparer plus facilement les nombreuses distances et méthodes de vote, nous proposons une vision plus locale du graphe, par rapport à un nœud donné.
En supposant qu'un nœud a été sélectionné dans le graphe global, il suffit ensuite de se rendre dans l'onglet *Local* de régler les paramètres puis de cliquer sur le bouton *Display*.

Le code calcule les k plus proches voisins pour chaque distance et méthode de vote sélectionnée, puis représente les points correspondants sur le graphe; la couleur d'un nœud correspond à la/les distance(s) pour laquelle il a été renvoyé. Le nœud central (ie la requête), est représenté par un carré vert. Comme pour le graphe global, on peut aussi cliquer sur les nœuds pour afficher plus d'information.
 Enfin le placement des points a deux modes:

  * **MDS**: Placement des points par l'algo de Multi Dimensional Scaling; la distance utilisée est la distance k-NN (pour k=2900, ie on calcule toutes les distances entre tous les points) qui a été symmétrisée (en faisant la moyenne des valeurs de part et d'autre de la diagonale).
  * **Circle**: Dans ce mode les distances sont normalisées entre 0 et 1; Placement des points en cercle autour du point central; le rayon pour un point est la moyenne de ses différentes distances (si une distance n'est pas renseignée, on suppose qu'elle vaut 1).

![Screen6](Screens/nodelocal.png)