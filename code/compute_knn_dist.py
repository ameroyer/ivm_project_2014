import logging 
logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__package__)

import os, sys, time
import random
import Image
import numpy as np
import scipy.sparse
from klists import Klist
from decoder.image import Imdec
from descriptor.gists import Gist
from indexation.exhaustive import ExhaustiveDb




################# ORIGINAL KNN ######################
def compute_knn(database_name, query_dir, images_dir, k = 100, sameset = False):
    """
    Given a database (.h5), the path to the queries and database images, to a results directory and a k parameter, this function outputs the k-nn distances computed for the queries against the database.
    """
    
    if sameset: #if query = database, query is itself a neighbour
        k = k + 1
    
    config = { 
        'common': {
            'dir': "./",
        },
        'Imdec': {
            'basedir': images_dir,
        },
        'Gist': {
            'image_resize': (64, 64), 
            'power_law': None, 
            'normalization': "L2", 
        },
        'ExhaustiveDb': {
            'drop_zero': True, 
            'knn': k, #number of nearest neighbors to retreive
            'disttype': "L2", 
            'dbfile': database_name,
        },
    }

    # to subsequently display the images, read all the file names from the
    # directory where all the images in the database were originally found.
    # !! strong assumption: no changes, same names, same order...
    DBimage_list = sorted([f for f in os.listdir(images_dir) if f.endswith(".jpg")])
    DB_N = len(DBimage_list)

    Qimage_list = sorted([f for f in os.listdir(query_dir) if f.endswith(".jpg")])
    Q_N = len(Qimage_list)


    image_factory = Imdec(config)
    gist_factory = Gist(image_factory, config)
    db = ExhaustiveDb(config)
    db.load()

    #Only record distances here
    resultsDists = np.zeros((Q_N, DB_N), dtype=np.float32)  # will record distances. NO distances can be exactly 0 here.

    #loop on all the query images and for each, get its k NN
    for qi_nb, query_image in enumerate(Qimage_list):
        print "%d / %d" %(qi_nb + 1, len(Qimage_list))
        d=list(gist_factory.getbyfile(query_dir+"/"+query_image))
        labels, dists = db.select(d[0]['desc'])
        resultsDists[qi_nb, labels] = dists
        resultsDists[qi_nb, qi_nb] = 0

    return resultsDists




###################################  Shared Neigbors Measure ###################################


#########################Compute Base Matrices
def shared_nearest_neighbors_sigm(resultsDists, k = 100):
    """
    Compute the base matrix for shared neighbours sigmoid
    """
    #resultsDists is assumed squarred
    n = len(resultsDists)
    new_resultsDists = np.zeros((n,n), dtype=np.float32)
    for i in range(n):
        print "%d/%d" %(i+1, n)
        for j in range(n-i):
            sigm = 1/(1+ np.exp(-resultsDists[i,i+j]/k))
            new_resultsDists[i,i+j]= sigm
        new_resultsDists[i,i]=0
        
    return new_resultsDists



def shared_nearest_neighbors_extended_sigm(resultsDists, k = 100):
    """
    Compute the base matrix for shared neighbours extended sigmoid
    """
    #resultsDists is assumed squarred
    n = len(resultsDists)
    klists = []
    for i in range(n):
        print "%d/%d" %(i+1, n)
        klists.append(Klist(k))
        for l in range(n):
                if resultsDists[i,l]!= 0:
                    klists[i].insert(resultsDists[i,l],l)
                    
    new_resultsDists = np.zeros((n,n), dtype=np.float32)
    for i in range(n):
        print "%d/%d" %(i+1, n)
        for j in range(n-i):
            print "%d/%d" %(j+1, n-i)
            for k_dash in range(k-1):
                snn = klists[i].inter(klists[i+j],k_dash+1)
                sigm = 1/(1+ np.exp(-float(k_dash+1-snn)/(k_dash+1)))
                new_resultsDists[i,i+j] += sigm/(k_dash+1)
        new_resultsDists[i,i]=0
        
    return new_resultsDists




def shared_nearest_neighbors(resultsDists, k = 100):
    """
    Compute the base matrix for shared neighbours
    """
    #resultsDists is assumed squarred
    n = len(resultsDists)
    new_resultsDists = np.zeros((n,n), dtype=np.float32)
    for i in range(n):
        print "%d/%d" %(i+1, n)
        for l in range(n):
                if resultsDists[i,l]!= 0:
                    for j in range(n-i):
                        if resultsDists[i+j,l]!= 0:
                            new_resultsDists[i,i+j] += 1 
        new_resultsDists[i,i]=0

    for i in range(n):
        for j in range(n-i):
            new_resultsDists[i,i+j] = k - new_resultsDists[i,i+j]
        
    return new_resultsDists



def shared_nearest_neighbors_set_cor(resultsDists, k = 100):
    """
    Compute base matrix for set corr shared neighbours
    """
    #resultsDists is assumed squarred
    n = len(resultsDists)
    new_resultsDists = np.zeros((n,n), dtype=np.float32)
    for i in range(n):
        print "%d/%d" %(i+1, n)
        for j in range(n-i):
            sc = float(n)/(n-k)*(float(resultsDists[i,i+j])/k-float(k)/n)
            new_resultsDists[i,i+j]= 1+sc
        new_resultsDists[i,i]=0
    return new_resultsDists



#################### Compute knn from base matrix
def compute_k_shared(sharedDists, k):
    """
    Compute sharedneighbours
    """
    n = len(sharedDists)
    newDists = np.zeros((n,n), dtype=np.float32)
    for i in range(n):
        print "%d/%d" %(i+1, n)
        l = Klist(k)
        for j in range(n-1):
            if j < i:
                l.insert(sharedDists[j,i],j)
            else:
                l.insert(sharedDists[i,j+1],j+1)
        le = l.get_le()
        li = l.get_li()
        for j in range(k):
            newDists[i,li[j]]= le[j]
    return newDists



############################### Distance based on Neighbor's neighbors #########################
def compute_one_iteration(resultsDists, allDists, k):
    n = len(resultsDists)
    newGraph = neighbors_graph_with_reverse(resultsDists)
    new_resultsDists = np.zeros((n,n), dtype=np.float32)
    for i in range(n):
        print "%d/%d" %(i+1, n)
        neigh = Klist(k)
        for j in range(n):
            if newGraph[i,j] != 0 and i != j:
                neigh.insert(allDists[i,j],j)
                for l in range(n):
                    if newGraph[j,l] != 0 and l != i:
                        neigh.insert(allDists[i,l],l)
                        
        le = neigh.get_le()
        li = neigh.get_li()
        for j in range(k):
            if le[j] == 0:
                print "Note: returned le[j] as knn"
                print i
                print li[j]
            new_resultsDists[i,li[j]]= le[j]
            
    return new_resultsDists



def neighbors_graph_with_reverse(resultsDists):
    n = len(resultsDists)
    newGraph = np.zeros((n,n), dtype=np.float32)
    for i in range(n):
        for j in range(n):
            newGraph[i,j] = resultsDists[i,j] + resultsDists[j,i]

    return newGraph
    


######################### Majority votes ######################################"
# resultsDists is a distance matrix (whatever the distance is
# k is the number of voters, i.e. the k-nn of the considered nodes
# filtred is the keeped ranking of neighboors in the output distance matrix
def election_with_majority(resultsDists,k = 100,filtred=1):
    n = len(resultsDists)
    newGraph = np.zeros((n,n), dtype=np.float32) 
    
    maxDists = resultsDists.max()
    resultsDistsWoZ = resultsDists
    resultsDistsWoZ[ 0==resultsDists ] = maxDists + 1 # Woz = Without Zeroes
    votes = resultsDistsWoZ.argmin(0) # votes(i) contains the vote (index of is 1-nn) of the nodes i 

    for i in range(n):
        allVoters = resultsDistsWoZ[i]
        voters = allVoters.argsort()
        for j in range(k):  # we only count the k better neighboors' vote
            newGraph[i,votes[voters[j]]] = newGraph[i,votes[voters[j]]] + 1
    # newGraph contains now the results of each vote
    
    for i in range(n):
        print "%d/%d" %(i+1, n)
        results = newGraph[i]
        ranking = np.argsort(results)[::-1] # we sort in descending order (best result first)
        ranking = ranking[::k]
        scores = sorted(results)[::-1]
        newGraph[i] = np.zeros(n, dtype=np.float32) # newGraph will now contains the ranking of the vote
        for j in range(len(ranking)):
            newGraph[i,ranking[j]] = j

    newGraph[newGraph>filtred] = 0
    return newGraph


def election_with_weighted_majority(resultsDists,k = 100,filtred=1):
    n = len(resultsDists)
    newGraph = np.zeros((n,n), dtype=np.float32) 
    
    maxDists =resultsDists.max();
    resultsDistsWoZ = resultsDists;
    resultsDistsWoZ[ 0==resultsDists ] = maxDists + 1 # Woz = Without Zeroes
    votes = resultsDistsWoZ.argmin(0) # votes(i) contains the vote (index of is 1-nn) of the nodes i 

    for i in range(n):
        allVoters = resultsDistsWoZ[i]
        voters = allVoters.argsort()
        for j in range(k):  # we only count the k better neighboors' vote
            newGraph[i,votes[voters[j]]] = newGraph[i,votes[voters[j]]] + 1/(1+ resultsDistsWoZ[i,voters[j]]) # the closer voter j is to i, the more important is vote is
    # newGraph contains now the results of each vote
    
    for i in range(n):
        results = newGraph[i]
        ranking = np.argsort(results)[::-1] # we sort in descending order (best result first)
        ranking = ranking[::k]
        scores = sorted(results)[::-1]
        newGraph[i] = np.zeros(n, dtype=np.float32) # newGraph will now contains the ranking of the vote
        for j in range(len(ranking)):
            newGraph[i,ranking[j]] = j

    newGraph[newGraph>filtred] = 0
    return newGraph




################################# Condorcet Vote ########################################
# resultsDists is a distance matrix (whatever the distance is
# k is the number of voters, i.e. the k-nn of the considered nodes
# listSize is the size of the vote list for each voters
#k_winners = number of non zero values for each line of the matrix
def election_with_condorcet(resultsDists,k = 100,listSize = 5, k_winners = 5):
    n = len(resultsDists)
    newGraph = np.zeros((n,n), dtype=np.float32) 
    
    maxDists =resultsDists.max();
    resultsDistsWoZ = resultsDists;
    resultsDistsWoZ[ 0==resultsDists ] = maxDists + 1 # Woz = Without Zeroes
    votes = resultsDistsWoZ.argsort(0) # votes(i) contains the vote (index of is n-nn) of the nodes i 
    
    for i in range(n):
        print "%d/%d" %(i+1, n)
        allVoters = resultsDistsWoZ[i]
        voters = allVoters.argsort() # voters are sorted according to their proximity to the node i
        duelVotes = np.zeros((n,n), dtype=np.float32) #duelVotes[i,j] contains all the win of i in the duel election between nodes i and j
        for j in range(k):  # we only count the k better neighboors' vote
            vote = votes[voters[j]]
            for l in range(listSize-1): # for each voter we simulate each duel
                candidat1 =vote[l]
                candidat2 = vote[l+1]
                duelVotes[candidat1,candidat2] = duelVotes[candidat1,candidat2] + 1
                duelVotes[candidat2,candidat1] = duelVotes[candidat2,candidat1] - 1
        victories = (duelVotes > 0)
        results = victories.sum(1)
        winner = results.argsort()[::-1] # the winner is the one with the more victories (Copeland's method)
        for l in range(k_winners):
            newGraph[i,winner[l]] = l+1
    return newGraph




def election_with_weighted_condorcet(resultsDists,k = 100,listSize = 5, k_winners = 5):
    n = len(resultsDists)
    newGraph = np.zeros((n,n), dtype=np.float32) 
    
    maxDists =resultsDists.max()
    resultsDistsWoZ = npresultsDists
    resultsDistsWoZ[ 0==resultsDists ] = maxDists + 1 # Woz = Without Zeroes
    votes = resultsDistsWoZ.argsort(0) # votes(i) contains the vote (index of is n-nn) of the nodes i 
    
    for i in range(n):
        print "%d/%d" %(i+1, n)
        allVoters = resultsDistsWoZ[i]
        voters = allVoters.argsort() # voters are sorted according to their proximity to the node i
        duelVotes = np.zeros((n,n), dtype=np.float32) #duelVotes[i,j] contains all the win of i in the duel election between nodes i and j
        weights = np.zeros(k, dtype=np.float32)
        for j in range(k):
            weights[j] = resultsDistsWoZ[i,voters[j]]

        weights = weights - weights.min()
        weights = np.exp(-(weights*weights)*10)

        for j in range(k):  # we only count the k better neighboors' votes
            vote = votes[voters[j]]
            weight =  weights[j] #1/(1+ resultsDistsWoZ[i,voters[j]])
            for l in range(listSize-1): # for each voter we simulate each duel
                candidat1 =vote[l]
                candidat2 = vote[l+1]
                duelVotes[candidat1,candidat2] = duelVotes[candidat1,candidat2] + weight
                duelVotes[candidat2,candidat1] = duelVotes[candidat2,candidat1] - weight
                
        victories = (duelVotes > 0)
        results = victories.sum(1)
        winner = results.argsort()[::-1] # the winner is the one with the more victories (Copeland's method)
        #print("node " + str(i) + "-->" + str(winner) + " score: " + str(resultsDists[i,winner]))
        for l in range(k_winners):
            newGraph[i,winner[l]] = l+1
    return newGraph



def election_with_weighted_restricted_condorcet(resultsDists,k = 100,listSize = 5, k_winners = 5):
    n = len(resultsDists)
    newGraph = np.zeros((n,n), dtype=np.float32) 
    
    maxDists = resultsDists.max()
    resultsDistsWoZ = resultsDists;
    resultsDistsWoZ[ 0==resultsDists ] = maxDists + 1 # Woz = Without Zeroes
    votes = resultsDistsWoZ.argsort(0) # votes(i) contains the vote (index of is n-nn) of the nodes i 
    
    for i in range(n):
        print "%d/%d" %(i+1, n)
        allVoters = resultsDistsWoZ[i]
        voters = allVoters.argsort() # voters are sorted according to their proximity to the node i
        duelVotes = np.zeros((n,n), dtype=np.float32) #duelVotes[i,j] contains all the win of i in the duel election between nodes i and j
        weights = np.zeros(k, dtype=np.float32)
        for j in range(k):
            weights[j] = resultsDistsWoZ[i,voters[j]]

        weights = weights - weights.min()
        weights = np.exp(-(weights*weights)*10)

        for j in range(k):  # we only count the k better neighboors' votes
            vote = votes[voters[j]]
            weight =  weights[j] #1/(1+ resultsDistsWoZ[i,voters[j]])
            number_of_votes = 0
            lastViewed = vote[0]
            l = 0
            while (number_of_votes < listSize): # for each voter we simulate each duel between candidat in the voters set
                if (vote[l+1] in voters):
                    candidat1 = lastViewed
                    candidat2 = vote[l+1]
                    #print("Voter " + str(voters[j]) + "("+ str(l) + "/" + str(k) + ")"  + "node i -->" + str(candidat1) + ": " + str(resultsDistsWoZ[voters[j],candidat1]) + "(" + str(weight) + ")" + "versus : node i -->" + str(candidat2) + ": " + str(resultsDistsWoZ[voters[j],candidat2]))
                    duelVotes[candidat1,candidat2] = duelVotes[candidat1,candidat2] + weight
                    duelVotes[candidat2,candidat1] = duelVotes[candidat2,candidat1] - weight
                    lastViewed = vote[l+1]
                    number_of_votes = number_of_votes + 1
                l = l +1
                
        victories = (duelVotes > 0)
        results = victories.sum(1)
        winner = results.argsort()[::-1] # the winner is the one with the more victories (Copeland's method)
        #print("node " + str(i) + "-->" + str(winner) + " score: " + str(resultsDists[i,winner]))
        for l in range(k_winners):
            newGraph[i,winner[l]] = l+1
    return newGraph






############## Main For Debugging Tests #########################################
if __name__ == "__main__":
    #configuration
    base_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    results_dir = base_dir + "/code/results/KNN_dists"
    vote_dir = base_dir + "/code/results/vote"
    shared_dir = base_dir + "/code/results/shared"
    iterate_dir = base_dir + "/code/results/KNN_iterate"
    images_dir = base_dir + "/Copydays/Database"
    query_dir = base_dir + "/Copydays/Database"
    database_name = base_dir + "/Database.h5"

    

    #### Calcul de la relation k-NN
    # resultsDists = compute_knn(database_name, query_dir, images_dir, k = 20, sameset= True)
    # np.save(os.path.join(results_dir, "20NN_dists.npy"), resultsDists)
    
    #resultsDists = compute_knn(database_name, query_dir, images_dir, k = 2900, sameset= True)
    #np.save(os.path.join(results_dir, "2900NN_dists.npy"), resultsDists)

    
    
    #### Calcul de shared avec 100kNN
    # resultsDists = np.load((os.path.join(results_dir, "100NN_dists.npy")))
    # results1 = shared_nearest_neighbors(resultsDists, k=100)
    # np.save(os.path.join(shared_dir, "100NN_shared.npy"), results1)

    
    
    #### sigm
    # sharedDists = np.load((os.path.join(shared_dir, "100NN_shared.npy")))
    # results1 = shared_nearest_neighbors_sigm(sharedDists, k = 100)
    # np.save(os.path.join(shared_dir, "100NN_shared_sigm.npy"), results1)


    
    #### sigm extended
    # print "here"
    # resultsDists = np.load((os.path.join(results_dir, "100NN_dists.npy")))
    # print "ici"
    # results1 = shared_nearest_neighbors_extended_sigm(resultsDists, k = 100)
    # np.save(os.path.join(shared_dir, "100NN_shared_sigm_ext.npy"), results1)


    #### test sigm
    # testMatrix = np.array(
    #     [[0,1,0,1,0,1,0],
    #      [0,1,0,1,0,1,0],
    #      [1,0,0,1,1,0,0],
    #      [1,1,0,0,0,0,1],
    #      [1,0,0,0,1,0,1],
    #      [0,0,0,1,1,0,1]])


    # results1 = shared_nearest_neighbors_extended_sigm(testMatrix, k = 3)
    # print results1

    

    #### set corellation

    # sharedDists = np.load((os.path.join(shared_dir, "100NN_shared.npy")))
    # results1 = shared_nearest_neighbors_set_cor(sharedDists)
    # np.save(os.path.join(shared_dir, "100NN_shared_set_cor.npy"), results1)
    
    # for i in range(10):
    #     print sharedDists[0,i+1]


    
    #### K-NN with shared distances
    
    # results1 = np.load((os.path.join(results_dir, "100NN_shared.npy")))
    # res = compute_k_shared(results1, k=10)
    # np.save(os.path.join(results_dir, "10NN_shared_k.npy"), res)

    
    ##### Test iteration neighbors
    # allDists = np.load((os.path.join(results_dir, "symmetrized_2900NN_dists.npy")))

    # resultsDists = np.load((os.path.join(results_dir, "20NN_dists.npy")))
    # newRes = resultsDists
    # np.save(os.path.join(iterate_dir+"/20NN_iterate", "iteration_0.npy"), newRes)    
    # for i in range(4):
    #    newRes = compute_one_iteration(newRes, allDists, k = 20)
    #    np.save(os.path.join(iterate_dir+"/20NN_iterate", "iteration_"+str(i+1)+".npy"), newRes)

    # resultsDists = np.load((os.path.join(results_dir, "10NN_dists.npy")))
    # newRes = resultsDists
    # np.save(os.path.join(iterate_dir+"/10NN_iterate", "iteration_0.npy"), newRes)    
    # for i in range(5):
    #    newRes = compute_one_iteration(newRes, allDists, k = 10)
    #    np.save(os.path.join(iterate_dir+"/10NN_iterate", "iteration_"+str(i+1)+".npy"), newRes)

    # resultsDists = np.load((os.path.join(results_dir, "5NN_dists.npy")))
    # newRes = resultsDists
    # np.save(os.path.join(iterate_dir+"/5NN_iterate", "iteration_0.npy"), newRes)    
    # for i in range(5):
    #    newRes = compute_one_iteration(newRes, allDists, k = 5)
    #    np.save(os.path.join(iterate_dir+"/5NN_iterate", "iteration_"+str(i+1)+".npy"), newRes)




    
    ##### Test vote 1
    
    # resultsDists = np.load((os.path.join(results_dir, "100NN_dists.npy")))
    # res = election_with_majority(resultsDists,100,100)
    # np.save(os.path.join(vote_dir, "100NN_majority.npy"), res)



    
    ##### Test vote 2
    
    # res = election_with_weighted_majority(resultsDists,100,100)
    # np.save(os.path.join(vote_dir, "100NN_weighted_majority.npy"), res)



    
    ##### Test vote 3
    
    #allDists = np.load((os.path.join(results_dir, "2900NN_dists.npy")))
    #resultsDists = np.load((os.path.join(results_dir, "5NN_dists.npy")))
    #res = election_with_condorcet(resultsDists,100)
    #np.save(os.path.join(base_dir + "/code/results/elections", "electionSimpleCondorcet.npy"), res)



        
    ##### Test vote 4
    
    #allDists = np.load((os.path.join(results_dir, "2900NN_dists.npy")))
    #resultsDists = np.load((os.path.join(results_dir, "100NN_dists.npy")))
    #res = election_with_weighted_condorcet(resultsDists,100,3)
   # np.save(os.path.join(base_dir + "/code/results/elections", "electionWeightedCondorcet.npy"), res)



   
    ##### Test vote 5
    
    #allDists = np.load((os.path.join(results_dir, "2900NN_dists.npy")))
    #resultsDists = np.load((os.path.join(results_dir, "100NN_dists.npy")))
    #res = election_with_weighted_restricted_condorcet(resultsDists,100,3)
   # np.save(os.path.join(base_dir + "/code/results/elections", "electionWeightedCondorcet.npy"), res)
