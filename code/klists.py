"""
KList structure for the shared neighbours
"""

import bisect

class Klist:
    def __init__(self, k):
        self.le = []
        self.li = []
        self.nb = 0
        self.k = k

    def get_li(self):
        return self.li

    def get_le(self):
        return self.le

    def insert(self, e, i):
        b = False
        j = 0
        while (not b) and j < self.nb:
            b = (self.li[j] == i)
            j +=1
        if not b:
            if self.nb <  self.k:
                j = bisect.bisect_left(self.le,e)
                self.li.insert(j,i)
                self.le.insert(j,e)
                self.nb += 1
            else:
                if not (self.le[self.k-1] <= e):
                    j = bisect.bisect_left(self.le,e)
                    self.li.insert(j,i)
                    self.le.insert(j,e)
                    self.le.pop()
                    self.li.pop()


    def inter(self, kl, k_dash):
        snn = 0
        for i in range(k_dash):
            for j in range(k_dash):
                if self.li[i] == kl.li[j]:
                    snn +=1
        return snn
