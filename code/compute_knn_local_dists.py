#Same as compute_knn_dists but compute knn for one point on the fly instead of computing the whole matrix
import logging 
logging.basicConfig(level=logging.DEBUG)
log = logging.getLogger(__package__)

import os, sys, time
import random
import Image
import numpy as np
import scipy.sparse
from klists import Klist
from decoder.image import Imdec
from descriptor.gists import Gist
from indexation.exhaustive import ExhaustiveDb


### Original kNN
def compute_knn_local(i, k):
    """
    Compute kNN for the ith query
    """
    log.propagate = False
    
    base_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    images_dir = base_dir + "/Copydays/Database"
    query_dir = base_dir + "/Copydays/Database"
    database_name = base_dir + "/Database.h5"
        
    config = { 
        'common': {
            'dir': "./",
        },
        'Imdec': {
            'basedir': images_dir,
        },
        'Gist': {
            'image_resize': (64, 64), 
            'power_law': None, 
            'normalization': "L2", 
        },
        'ExhaustiveDb': {
            'drop_zero': True, 
            'knn': k, #number of nearest neighbors to retreive
            'disttype': "L2", 
            'dbfile': database_name,
        },
    }

    DBimage_list = sorted([f for f in os.listdir(images_dir) if f.endswith(".jpg")])
    DB_N = len(DBimage_list)
    Qimage_list = sorted([f for f in os.listdir(query_dir) if f.endswith(".jpg")])

    image_factory = Imdec(config)
    gist_factory = Gist(image_factory, config)
    db = ExhaustiveDb(config)
    db.load()

    #Only record distances here
    resultsDists = np.zeros((1, DB_N), dtype=np.float32)  
    query_image = Qimage_list[i]
    d=list(gist_factory.getbyfile(query_dir+"/"+query_image))
    labels, dists = db.select(d[0]['desc'])
    resultsDists[0, labels] = dists
    resultsDists[0, i] = 0
    return resultsDists



def compute_k_shared_local(i, k):
    """
    Compute kNN shared neighbours for the ith query
    """
    base_dir = os.path.dirname(os.path.dirname(os.path.realpath(__file__)))
    results_dir = base_dir + "/code/results/"
    sharedDists = np.load((os.path.join(results_dir, "50NN_shared_sigm.npy")))
    n = len(sharedDists)
    newDists = np.zeros((1,n), dtype=np.float32)
    l = Klist(k)
    for j in range(n):
        if j < i:
            l.insert(sharedDists[j,i],j)
        elif j > i:
            l.insert(sharedDists[i,j],j)
    le = l.get_le()
    li = l.get_li()
    for j in range(k): 
        newDists[0,li[j]]= le[j]
    newDists[0,i] = 0
    return newDists
