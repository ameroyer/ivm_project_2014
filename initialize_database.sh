dir=$(dirname $(readlink -f $0))
database=$dir/visu/Database/
mkdir $database

original=$dir/Copydays
cp $original/Queries/* $database
cp $original/Distractors/* $database
cp $original/Originales/strong* $database
cp $original/Originales/crops_50* $database
